# Delivery 6 / 12.09.2019

## Main changes and updates

+ **All styles ported from SASS to LESS** for compatibility with the toolkit and the Angular project requirements. Bootstrap is pre-compiled. Custom variables are overrided. (Important: Post-CSS plugins needed: autoprefixer, css-declaration-sorter, cssnano. Oscar, you will find these and their options in the nuxt.config.js file) Less files can be found in **dev-files/less**. SASS mixins were not compiled in the Angular project (also functions). Specially media-queries. This caused, maybe, some of the graphical bugs we found on Angular.
+ **assets** folder : some new images or icons (these are temporary)
+ **ipex.css has then been removed** from this ipex-html repo because compiled in Angular project (Important: Post-CSS plugins needed: autoprefixer, css-declaration-sorter, cssnano ) to avoid misunderstandings.
+ **Card scrutiny component** (additional classes on tags see commit on 09/09/19)
+ **.page-pictures** class replaced by **.visual-layout** class in parliament-details page
+ **Alignment changes with thin cards** (with no descriptions, as we may find in Landing page model 2, list of meetings in Conference page, and this test site homepage). Landing 2 may be used as a reference.
+ **.ipx-search** options removed from mobile-meu. **Search .nav-item has been added on mobile devices** (should link to the main search page)
+ **Main menu** logic has changed. Freeded from Bootstrap navbar open/close. I guess this will be directly managed with Angular or Toolkit. So main menu markup may have slightly changed. Needs to be updated and tested.
+ Update of **content/list-of-files-collapse**.

![](assets/img/conf.jpg)

## Templates (pages folder)

+ Conference layout - ([test here](https://ipextest.netlify.com/conferences/conference/))

The way we will manage the **personalization of the conferences** (backgrounds, colors, logos ..) needs to be discussed with the team. Currently hard-coded but this should change.

## Components

+ New **Collapsible Files list** (content/list-of-files-collapse - **UPDATED**)
+ **Collapsible Content Header** (content/conferences/content-header-conferences)
+ **Conference menu** (content/conferences/conference-menu)
+ **Content conference** (content/conferences/content-conference)
+ **Home tab** (content/conferences/home)
+ **Meeting tab** (content/conferences/meeting)
+ **Meetings list** (content/conferences/meetings-list)
+ **About tab** (content/conferences/about)

## Test URL

You can access the different pages that are done at this address: [https://ipextest.netlify.com](https://ipextest.netlify.com) (All from the Homepage - Navigation is not done).

# Delivery 5 / 30.08.2019

## Updates on previous deliveries

+ Removed **related documents from Scrutiny page** (seen with Majdi)
+ **Removed Parliaments icon** from search module (in main menu)
+ **ipex.css**
+ **assets folder** (images and icons)

## Test URL

You can access the different pages that are done at this address: [https://ipextest.netlify.com](https://ipextest.netlify.com) (All from the Homepage - Navigation is not done).

## Templates (pages folder)

+ Homepage - ([test here](https://ipextest.netlify.com/home/))
+ Parliament details layout - ([test here](https://ipextest.netlify.com/parliaments/parliament-details/))
+ Search documents (temporary for discussion, on IPEXTEST) - ([test here](https://ipextest.netlify.com/documents/legislative-database/))

## Components

+ **Content header for Parliament** details (content-header-parliament)
+ **Content for Parliament details** (content-parliament-details)
+ **Bordered box** for Parliament details (and conferences) (bordered-box)
+ Carousel **Gallery** for Parliament details (Carousel Gallery)
+ **Links block for Parliament details** (list-of-links-parliament)
+ **Bordered button** (View more on homepage - Regular and large)
+ Homepage: Homeblock (base, for info)
+ Homepage: **Title block** for modules, with select
+ Homepage: ***Information to share* block** (wraps "Featured" Carousel and Carousel of cards)
+ Homepage: **"Featured" Carousel** for *Information to share* block, with controls (infotoshare-featured.html)
+ Homepage: Carousel of cards for *Information to share* block
+ Homepage: **Cards for *Information to share*** block
+ Homepage: Carousel of cards for *High activity* block
+ Homepage: **Cards for *High activity*** block
+ Homepage: Carousel of cards for *Upcoming events* block
+ Homepage: **Cards for *Upcoming events*** block
+ Homepage: Carousel of cards for *News* block
+ Homepage: **Cards for *News*** block
+ Homepage: **Video Block** (with Youtube integration)
+ Homepage: **Subscriptions block**

## To clarify

+ Datepicker component design (check with toolkit)
+ Les tags dans les Information to share sont cliquables?
+ Il faut penser à comment réceptionner ls images, et faire une liste images et icones
+ Browserlist : tout aligner
+ We need to make the "Go to top" working
+ (Homepage small devices to fix)

# Delivery 4 / 08.08.2019

## Updates on previous deliveries

+ **assets** folder has been updated
+ **ipex.css** has been updated
+ **SASS files** have been updated (dev-files)
+ **List of files** component has been updated (+ **list-of-files-collapse** has been added)
+ **Container classes** and widths may have changed (no impact on functionalities) This will be discussed later.
+ **List of Secretaries General** has been updated (.chamber-name class added)
+ **List of languages** tags document
+ **Load more button** : classes from html have been removed
+ **.results-tag** class should be replaced everywhere by **ipx-tag** (little colored boxes for scrutinies and documents)

NOTE: I can update those elements myself so I do not give you extra work, but I need a way to get the Angular project running.

## New design test URL

You can access the different pages that are done at this address: [https://ipextest.netlify.com](https://ipextest.netlify.com) (All from the Homepage - Navigation is not working).

Natalia, this may be a reference for you in the future, as it will be always up to date and is more flexible than static screens.

## Templates (pages folder)

+ **Create an account** (My IPEX) - ([test here](https://ipextest.netlify.com/myipex/create-an-account))
+ **Manage my account** (My IPEX) - ([test here](https://ipextest.netlify.com/myipex/manage-my-account))
+ **Document layout** (pages/documents/document) - ([test here](https://ipextest.netlify.com/documents/document)) Yellow/Orange card documents just need an additional class. See comments.
+ **Documents with high activity page** (pages/documents/high-activity) - ([test here](https://ipextest.netlify.com/documents/high-activity))
+ **Scrutiny layout** (pages/documents/scrutiny) - ([test here](https://ipextest.netlify.com/documents/scrutiny))
+ **Dossier page layout** (pages/documents/dossier) - ([test here](https://ipextest.netlify.com/documents/dossier))

## Components
+ **list-of-files-collapse** has been added
+ **List with bullets**
+ **Page divider**
+ **Lisbon block** used in scrutiny page
+ **Content header for a document**
+ **Document activity** block (with ipx-tags)
+ **Timeline for document** (components/documents)
+ **First section for document** (specific layout)
+ **Card for scrutinies** (little card in scrutinies section on a document)
+ **Filter for Scrutinies**
+ **Scrutinies** (list of scrutinies in a document) (components/documents/Scrutinies)
+ **External contents** (links with logos for Regpex, Oeil and Eurlex) (components/documents/ExternalContents)
+ **List of languages** (components/documents/ListLanguages)
+ **Classification block** (bootstrap columns) used in document page
+ **Content header for a scrutiny**
+ **Timeline for scrutinies** (components/documents)
+ **First section for scrutiny** (specific layout)
+ **Card for High activity** (use for Dossier also)
+ **gdpr-general** GDPR block for first access to the website (mandatory)
+ **gdpr-in-page** GDPR block for manage my account page (reminder)

## Todo list

+ Progress bars will have to be changed (only CSS)
+ **Calendar of events**: datepicker and Date range module should be added but will be checked first with the MCC toolkit.

------

# Delivery 3(part 2) 31.07.2019 (D3-2)

## Updates on previous deliveries

+ **List of parliaments**: Slightly inscreased the size of flags (flag-sm instead of flag-xs) and their proportions (fixing a bug with some as Ireland)
+ Of course **ipex.css**

## Templates (pages folder)

+ **IPEX Board** (pages/list-of-contacts) - ([test here](https://ipexhtml.netlify.com/html/pages/list-of-contacts))
Layout for a list of persons in categories
+ **Secretaries general** (pages/list-of-contacts2) - ([test here](https://ipexhtml.netlify.com/html/pages/list-of-contacts2))
Layout for a list of countries with their respective chambers and contacts. Used for Secretaries general and Correspondents
+ **List of results** (pages/list-of-results) - ([test here](https://ipexhtml.netlify.com/html/pages/list-of-results))
ONGOING. I need to see first with Chalee for the impact of the advanced search on this. (criteria, refining options etc..)

Please note that, in those pages, header, footer and other head sections are all the same for all pages and therefore cannot be used as references. **Please focus on .ipx-intro and .ipx-content sections. It is where specific page elements are inserted**.

## Components

+ **Card for a person**, used for IPEX board (components/cards/card-person)
+ **Card for a country**, used for Secretaries general and Correspondents (components/cards/card-country)
+ **IPEX Board Intro** is a particular format used probably only here (components/content/board-intro)
+ **Modal box**, used in list of results (components/content/modal)

## Screens

+ Screens for each page/template, including mobile devices. (not real devices, that are still to be tested). All are stored in the **/screens** folder. Those screens may be use as **a reference for the design and graphical integration**. **Implementation should match these screens**.

## Data

+ I have created some **JSON files for testing** with some data for countries, contacts etc.. I don't know if it may be useful for others, so I created a "data" folder in this repository, where I will put those JSON files. (for testing only!)

# Delivery 3(part 1) 26.07.2019 (D3-1)

## Updates on previous deliveries

The following files/components have been updated:

+ template (/html/template) : **Fancybox** script has been added for enlarging images.
+ Card lists (/components/content/cards-lists) : update + adding the 4 columns layout for parliaments.
+ Calendar page has been updated (specially search input and filters). 
+ html/pages folder has been deleted
+ html/testing folder has been renamed to html**/pages**. This folder contains pages for testing where you can grab pieces of code if you do not find something in the html**/components** folder.
+ The components folder contains the different blocks of code, in categories. This should be, along the screenshots, the reference for implementation.

> WARNING: Please, when implementing, try to keep the code as close as possible to what is delivered here. Changes in the markup may bring to broken layouts or issues in margins/paddings that could have a serious impact on the design, especially on mobile devices.

## Templates (pages folder)
Please note that, in those pages, header, footer and other head sections are all the same for all pages and therefore cannot be used as references. **Please focus on .ipx-intro and .ipx-content sections. It is where specific page elements are inserted**.

+ **Calendar of events** - updated (pages/calendar) - ([test here](https://ipexhtml.netlify.com/html/pages/calendar))
+ **Event details** (pages/details-event) - ([test here](https://ipexhtml.netlify.com/html/pages/details-event))
+ **List of parliaments** (pages/list-of-parliaments) - ([test here](https://ipexhtml.netlify.com/html/pages/list-of-parliaments))
+ **List of news** (pages/list-of-news) - ([test here](https://ipexhtml.netlify.com/html/pages/list-of-news))
+ **News details 1** (pages/details-news) - ([test here](https://ipexhtml.netlify.com/html/pages/details-news))
+ **News details 2** (pages/details-news2) - ([test here](https://ipexhtml.netlify.com/html/pages/details-news2))
+ **List of stored searches** (pages/list-of-stored-searches) - ([test here](https://ipexhtml.netlify.com/html/pages/list-of-stored-searches))
+ **Stored search details** (pages/details-stored-search) - ([test here](https://ipexhtml.netlify.com/html/pages/details-stored-search))

## Components

+ **Card for an event**. With indicative dates badge. (components/cards/card-event)
+ **Card for parliament**, with links inside the card (components/cards/card-parliament)
+ **Large filter input** (for filtering) - (components/forms/large-input-with-button)
+ **Large filter input with button** (for searching) - (components/forms/large-input-with-button)
+ **Filters box** (update) - (components/forms/filters)
+ **Filters box for news** (components/forms/filters-news)
+ **Expandable thumbnail** using fancybox (components/content/expandable-thumbnail)
+ **Multipage Pagination** component (see My stored searches page) - (components/navigation/pagination)
+ **Basic text/image** component (Quote image), used in the parliaments list page. (components/content/quote-image)
+ **Back content header** (used in details pages, with "back to list" button) - (components/content/back-content-header)
+ **Bottom navigation** (used in details pages) - (components/navigation/bottom-navigation)
+ **Details page intro** (components/content/details-page-intro)
+ **List of links** (components/content/list-of-links)
+ **List of files** (could be expandable) - (components/content/list-of-files)

## Screens

+ Screens for each page/template, including mobile devices. (not real devices, that are still to be tested). All are stored in the **/screens** folder. Those screens may be use as **a reference for the design and graphical integration**. **Implementation should match these screens**.

------

# Delivery 2 26.06.2019 (D2)

## Updates on D1

The following files/components have been updated:

+ html/content/basic-content-header
+ html/template.html (update of jQuery version + and meta tags)
+ css/ipex.css

Following Chrome audits, I did some changes, particularly breadcrumb and btn-rounded in site-params (language and user menu). Results are very good, even if we should manually check several points to improve accessibility.

![Audit](/assets/dev/audit1.png)

## Templates (pages folder)

+ Landing page model 1 ([test here](https://ipexhtml.netlify.com/html/testing/landing))
+ Landing page model 2 ([test here](https://ipexhtml.netlify.com/html/testing/landing2))
+ Calendar page (ongoing) ([test here](https://ipexhtml.netlify.com/html/testing/calendar))
+ Cards layouts ([test here](https://ipexhtml.netlify.com/html/testing/demo))

## Components

+ IPEX widget (content)
+ Basic card
+ Basic card with footer
+ Basic card with marks (used in search results)
+ Card with an image (icon, picture..)
+ Card with a contextual menu
+ Card for documents
+ Card for events
+ Card for conferences (landing page)
+ Card thin (only title)
+ Cards list basic markup
+ Cards list 1 column (content/cards-lists)
+ Cards list 2 columns (content/cards-lists)
+ Cards list 3 columns (content/cards-lists)
+ Forms: Large search field (forms)
+ Forms: Calendar filters (forms)
+ Carousel component for cards (cards)

## Screens

+ Screens for each page/template, including mobile devices. (not real devices, that are still to be tested)
+ Screens of this D2 are stored in screens/calendar, screens/cards and screens/landing-pages

------

# Delivery 1 12.06.2019 (D1)

## 12.06.2019 / Contenu

+ Template de base
+ Header
+ Ipex logo
+ User menu
+ Language menu
+ Language mobile (marquage différent)
+ Main menu
+ Main search
+ Breadcrumbs
+ Footer
+ Go to top
+ Content header
+ Content
+ Ecrans, notamment pour les périphériques mobiles.

## 12.06.2019 / Comentaires
 
+ Le **split bootstrap** dans une feuille de style à part viendra par la suite (ce ne sera pas un problème)
+ Le dossier **pages** reprendra les contenus spécifiques
+ Les contenus, libellés et attributs de type *alt* ou *title* sont à remplacer par ceux qui conviennent (en provenance de la base de données ou d'ailleurs) ainsi que l'ensemble des URLs présentes dans les liens des menus.
+ La présence de certains items dans le **user menu** doit être conditionnée, par exemple, par le *role* de l'utilisateur, s'il est loggé ou non, etc.
+ Le dossier **testing** n'est, comme son nom l'indique, destiné qu'au test graphique.
+ Le **go to top** ne fonctionne pas encore. Il faut ajouter le JS.
+ La classe **section-ipex**, sur le main-wrapper, devrait changer en fonction de la section dans laquelle on se situe (section-calendar, section-documents, section-parliaments, etc..)
+ Le dossier **screens** contient des captures d'écrans des pages et/ou des composants. Ils sont là pour donner une idée du rendu, notamment en ce qui concerne les périphériques mobiles.
+ Il faudra prendre le temps du test sur les différents navigateurs. Je n'ai pour l'instant testé que dans **Firefox 67.0.1** et **Chrome 74.0.3729.169**
+ Des ajustements viendront sans aucun doute dans le futur.